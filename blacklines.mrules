// This Maperitive ruleset is based on the default 'Google Maps' ruleset, that ships with maperitive, created by Igor Brejc and updateded by Michael <quelbs_at_gmail.com>. It's heavily simplified and some features are not actually used in the rules afterwards.
// Released under the Creative Commons Attribution-ShareAlike 3.0 License (http://creativecommons.org/licenses/by-sa/3.0/)

// CHANGELOG
// 281019: 	- Made the whole design a bit thinner by decreasing thickness and chosing a dark gray instead of black for highways and major roads
//			- Made highways and roads dashed and gray when tunnel = yes 
//			- Cleaned up and simplified airports 
//			- added rail=light_rail to railway 
//			- deleted features and properties not in use			


features


	lines
		boundary country : boundary=administrative AND (admin_level=2 OR admin_level=4) AND NOT natural=coastline

		aeroway line runway : aeroway=runway
		aeroway line taxiway : aeroway=taxiway

		railway : (railway=rail OR railway=light_rail) AND @isFalse(disused) AND NOT service=yard AND @isFalse(noexit)
		
		motorway : highway=motorway
		motorway link : highway=motorway_link
		major road : @isOneOf(highway, trunk, trunk_link, primary, primary_link, secondary, tertiary)
		minor road : @isOneOf(highway, unclassified, residential, service, living_street) OR (highway=track AND (@isOneOf(tracktype, grade1, grade2, grade3) OR NOT tracktype))
		path : @isOneOf(highway, path, footway, pedestrian, steps) OR (highway=track AND @isOneOf(tracktype, grade4, grade5))

		water line : waterway=stream OR waterway=river

	areas
		water : natural=water OR natural=wetland OR waterway=riverbank OR waterway=stream OR landuse=reservoir OR landuse=basin OR natural=bay
		aeroway area : aeroway


properties
	map-background-color	: #FFFFFF
	map-background-opacity	: 1
	map-sea-color : white
	//not sure what these things do
	map.rendering.lflp.min-buffer-space : 5
	map.rendering.lflp.max-allowed-corner-angle : 40

rules


// lines

	target : boundary*
		define
			line-color : #818181
			line-width : 2
			border-style : solid
			border-color : #818181
			border-width : 110%
			border-opacity : 0.4
		draw : line

	target : aeroway line*
		define
			min-zoom : 9
			line-color : gray
		if : aeroway line runway
			define
				line-width : 9:1;10:1;11:2;13:4;15:10
		else
			define
				line-width : 9:1;11:1;13:3;15:10
		draw : line

	target : railway
		define
			min-zoom : 13
			line-color : #a1a1a1
			line-width : 2
		draw : line
		define
			min-zoom : 13
			line-style : dashlong
			line-color : white
			line-width : 1
			border-style : solid
			border-color : #a1a1a1
			border-width : 25%
		draw : line
		define
			min-zoom : 6
			max-zoom : 13
			line-style : solid
			border-style : none
			line-color : #a1a1a1
			line-width : 1
		draw : line

	target : motorway
		define
			line-width : 7:1;13:1;15:5
			min-zoom : 7
			line-color : black
		for : tunnel=yes
			define
				line-style : dash
				line-color : gray
		draw : line


	target : motorway link
		define
			line-width : 7:1;13:1;15:5
			min-zoom : 10
			line-color : black
		for : tunnel=yes
			define
				line-style : dash
				line-color : gray
		draw : line

	target : major road
		define
			min-zoom : 8
			line-color : #101010
			line-width : 10:0.7;13:1;14:2;15:3;18:6

		for : highway=tertiary
			define
				min-zoom : 11
		for : tunnel=yes
			define
				line-style : dash
				line-color : gray
		draw : line


	target : minor road
		define
			min-zoom : 10.5
			max-zoom : 13.1
			line-color : gray
			line-width : 0.8
		draw : line
		define
			min-zoom : 13.1
			max-zoom : 20
			line-color : gray
			line-width : 13:0.8;14:2;15:3;16:6
			line-end-cap: round

		draw : line
		for : tunnel=yes
			define
				line-style : dot
		draw : line


	target : path
		define
			min-zoom : 14.5
			max-zoom : 20
			line-color : lightgray
			line-width : 14:1;15:1;15.5:3
		for : tunnel=yes
			define
				line-style : dot
		draw : line

	target : water line
		define
			min-zoom : 10
			line-color : white
			line-width : 14:1;16:5;20:10
		draw : line

// landuse

	target : $featuretype(area)
		define
			line-style : none
			line-width : 1
		if : water
			define
				fill-color : white
		elseif : aeroway area
			define
				fill-color : white
		else
			stop
		draw : fill


